import { fromJS, List, Map } from 'immutable'

const majorKeys = List([
  'C',
  'Db',
  'D',
  'Eb',
  'E',
  'F',
  'Gb',
  'G',
  'Ab',
  'A',
  'Bb',
  'B'
])

export async function randomChord (options) {
  const constOptions = fromJS(options) || Map()
  const keysToChooseFrom = majorKeys.filterNot(
    v => constOptions.get('excludeChords', List()).includes(v))
  const randomIndex = Math.floor(Math.random() * keysToChooseFrom.size)
  return keysToChooseFrom.get(randomIndex, 'N/C')
}
